arch snes.cpu

define DATA_SPC_BIN "spc.bin"

incsrc "include_snes/snes.asm"
incsrc "include_snes/common.asm"
incsrc "midi_in_snes.asm"

define old_joy_status $C0
define key_on_joy $C2
define key_off_joy $C8
define cur_octave $CC
define note_shift $CE
define note_shift2 $CD
define midi_note $CF
define key_offset $D0

macro send_midi_to_spc s1,s2,s3
    lda.b {s1}
    sta {APU_PORT1}
    lda.b {s2}
    sta {APU_PORT2}
    lda.b {s3}
    sta {APU_PORT3}
    {spc_strobe}
endmacro

main:
   jsr RedScreen

   {spc_load SPCUpload}

   jsl midi_input_init

   {A_BYTE}
   lda.b #$04
   sta {cur_octave}
   stz {note_shift}
   stz {note_shift2}

  RunLoop:
    if {defined MIRACLE_PIANO_MODE}
      jsr read_midi
    else
      jsr read_joy
    endif
  bra RunLoop

read_joy:
  php
  jsl read_joy1_status
  {A_WORD}
  {XY_BYTE}
  lda {joy_status}
  eor {old_joy_status}
  and {joy_status}
  sta {key_on_joy} 
  lda {joy_status}
  eor {old_joy_status}
  and {old_joy_status}
  sta {key_off_joy}
  lda {joy_status}
  sta {old_joy_status}

  jsr parse_key_on

  jsr parse_key_off

  plp
  rts

parse_key_on:
  ldx.b #$00

  -;
  lda {key_on_joy}
  ror
  bcc .next2
    jsr handle_key_on
  .next2:
  sta {key_on_joy}
  inx
  cpx.b #$10
  bcc {-}
  rts

handle_key_on:
    pha
    {A_BYTE}
    lda key_index,x
    cmp.b #$FF
    beq .skip2
    cmp.b #$0D
    bcs .skip1
      jsr calc_note
      bra .skip2
    .skip1:
      cmp.b #$21
      beq .octup
      cmp.b #$20
      beq .octdn
      cmp.b #$10
      beq .shift1
      cmp.b #$11
      bne .skip2

        lda.b #$0C
        sta {note_shift2}
        bra .skip2

      .shift1:
        lda.b #$01
        sta {note_shift}
        bra .skip2

      .octup:
        lda {cur_octave}
        inc
        cmp.b #$08
        bcc .safeA
          lda.b #$08
        .safeA:
          sta {cur_octave}
        bra .skip2
      .octdn:
        lda {cur_octave}
        beq .skip2
        dec
        sta {cur_octave}
        bra .skip2

    .skip2:
    {A_WORD}
    pla
  rts

calc_note:
      sta {key_offset}
      lda {cur_octave}
      asl
      asl
      asl
      sta {midi_note}
      lda {cur_octave}
      asl
      asl
      clc
      adc {midi_note}
      clc
      adc {key_offset}
      clc
      adc {note_shift}
      clc
      adc {note_shift2}
      cmp.b #$80
      bcc .safe
      lda.b #$7F
      .safe:
      sta $CF

      {send_midi_to_spc #$90, {midi_note}, #$7F}

      rts

parse_key_off:
  ldx.b #$00

  -;
  lda {key_off_joy}
  ror
  bcc .next
    jsr handle_key_off
  .next:
  sta {key_off_joy}
  inx
  cpx.b #$10
  bcc {-}

  rts


handle_key_off:
    pha
    {A_BYTE}
    lda key_index,x
    cmp.b #$10
    bne .notshift
    stz {note_shift}
    bra .skip
    .notshift:
    cmp.b #$11
    bne .notshifte
    stz {note_shift2}
    bra .skip
    .notshifte:
    cmp.b #$0D
    bcs .skip
      {send_midi_to_spc #$90, #$3C, #$00}
    .skip:
    {A_WORD}
    pla
  rts

key_index:
  // DR  DL  DD  DU  ST  SL  Y   B
  db $05,$00,$02,$04,$21,$20,$07,$09

  // XX  XX  XX  XX  R   L   X   A
  db $FF,$FF,$FF,$FF,$11,$10,$0B,$0C


read_midi:
  php
  jsl read_midi_byte
  {A_BYTE}
  lda {midi_in_flag} // received a full message?
  beq .nomsg

    //forward msg to SPC
    {send_midi_to_spc {midi_in_msg}, {midi_in_msg}+1, {midi_in_msg}+2}

    jsl midi_cmd_init

  .nomsg:

  plp
  rts


RedScreen:
   lda.b   #%00011111
   sta   $2122
   lda.b   #%00000000
   sta   $2122
   lda.b   #%00001111
   sta   $2100
   rts

{seek $10000}
SPCUpload:
  incsrc "out/spcinc_snes.asm"
