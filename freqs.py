
cf = 0x1000
cn = 60

freqs = [min([0x3FFF,int(float(cf) * 2. ** (float(i-cn) / 12.))]) for i in xrange(128)]
outs = ""
for i in xrange(128):
  if i % 12 == 0:
    outs += "\ndw "
  else:
    outs += ","
  outs+= "0x%04X" % freqs[i]
print outs 