
{seek $3C00}
  sample_dir:
  dw sample0, sample0+$65d
  {ExternSPCFragment sample_dir}


{seek $3D00}
  inst_dir:
  db 00
  {ExternSPCFragment inst_dir}

{seek $3E00}
  pitch_dir:
    dw 0x1000
  {ExternSPCFragment pitch_dir}

{seek $4000}
  samples:
    sample0:
      incbin "sample.brr"

  {ExternSPCFragment samples}

