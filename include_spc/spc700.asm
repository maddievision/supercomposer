endian lsb

if {defined SPC_FILE}

  org $0000
  base $0000
  fill $101C0
  org $0000

  db "SNES-SPC700 Sound File Data v0.30"
  db 26,26
  db 26     //with ID666 Tag
  db 30

  //initial registers
  dw Start  //PC
  db 0      //A
  db 0      //X
  db 0      //Y
  db 0      //PSW
  db 0      //SP
  db 0      //reserved

  //ID666 Tag
  org $2E; db "No Limit SPC-700 Engine"
  org $4E; db "No Limit Zone"
  org $6E; db "No Limit Zone"
  org $7E; db "No Comment"
  org $9E; db "01/01/2000"
  org $A9; db "999"
  org $AC; db "1000"
  org $B1; db "No Limit Zone"
  org $D1; db 0,0

  macro extern name
      endian lsb
  endmacro

  macro ExternSPCFragment start
      endian lsb
  endmacro

  macro ExternSPCVector addr
      endian lsb
  endmacro

  macro seek n
    org {n} + 0x100
    base {n}
  endmacro

else
  macro extern name
      print "define {name} {$}"
  endmacro

  macro ExternSPCFragment start
      align 4
      eval astart {start}
      eval len {$}-{start}
      print "{SPCFragment {DATA_SPC_BIN}, {astart}, {len}}"
  endmacro

  macro ExternSPCVector addr
      eval aaddr {addr}
      print "{SPCVector {aaddr}}"
  endmacro


  macro seek n
    org {n}
    base {n}
  endmacro
  {seek $0}
  fill $10000
  {seek $0}


endif



//SPC Registers
define CONTROL $F1

define DSP_ADDR $F2
define DSP_DATA $F3

define PORT0 $F4
define PORT1 $F5
define PORT2 $F6
define PORT3 $F7

define TIMER0 $FA
define TIMER1 $FB
define TIMER2 $FC

define COUNTER0 $FD
define COUNTER1 $FE
define COUNTER2 $FF

//DSP Channel Registers

define DSP_REG_CH_VOL_L $00
define DSP_REG_CH_VOL_R $01
define DSP_REG_CH_P_L $02
define DSP_REG_CH_P_R $03
define DSP_REG_CH_SRCN $04
define DSP_REG_CH_ADSR1 $05
define DSP_REG_CH_ADSR2 $06
define DSP_REG_CH_GAIN $07
define DSP_REG_CH_ENVX $08
define DSP_REG_CH_OUTX $09

//DSP Registers

define DSP_REG_MVOL_L $0C
define DSP_REG_MVOL_R $1C
define DSP_REG_EVOL_L $2C
define DSP_REG_EVOL_R $3C
define DSP_REG_KON $4C
define DSP_REG_KOF $5C
define DSP_REG_FLG $6C
define DSP_REG_ENDX $7C

define DSP_REG_EFB $0D
define DSP_REG_PMON $2D
define DSP_REG_NON $3D
define DSP_REG_EON $4D
define DSP_REG_DIR $5D
define DSP_REG_ESA $6D
define DSP_REG_EDL $7D

macro dsp_write
  movw {DSP_ADDR}, ya
endmacro

define STACK_BASE $CF

macro spc_init
  clrp
  mov x, #{STACK_BASE}
  mov a, #$00
  mov x,a
  -; 
    mov (x)+,a
    cmp x, #$e0
    bne {-}

  mov x, #$00
  -;
    mov $0200+x,a
    inc x
    bne {-}

  -;
    mov $0300+x,a
    inc x
    bne {-}

  //MVOL
  mov a, #{DSP_REG_MVOL_L}
  mov y, #$60
  {dsp_write}
  mov a, #{DSP_REG_MVOL_R}
  {dsp_write}

  mov a, #{DSP_REG_DIR}
  mov y, #$3C
  {dsp_write}

  mov a, #{DSP_REG_NON}
  mov y, #$00
  {dsp_write}


  mov a, #{DSP_REG_EVOL_L}
  mov y, #$00
  {dsp_write}
  mov a, #{DSP_REG_EVOL_R}
  mov y, #$00

  {dsp_write}

  mov a, #{DSP_REG_EFB}
  mov y, #$00
  {dsp_write}

  mov a, #{DSP_REG_EON}
  mov y, #$00
  {dsp_write}

  mov a, #{DSP_REG_ESA}
  mov y, #$F0
  {dsp_write}

  mov a, #{DSP_REG_EDL}
  mov y, #$00
  {dsp_write}



  mov a, #{DSP_REG_FLG}
  mov y, #$20
  {dsp_write}

endmacro

{seek $0800}

Start: 
  {spc_init}
  jmp Main