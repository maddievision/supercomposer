incsrc "include_spc/spc700.asm"

//Program Variables

define arg0 $40
define arg1 $41
define arg2 $42
define arg3 $43

define tmp0 $50
define tmp1 $51
define tmp2 $52
define tmp3 $53

//Time inbetween Note Off and Note On (cycles)
define waitperiod $36

define tickspeed $60
define ticker $62
define strobe_in $30

Main:
  //clear message reading
  mov {strobe_in}, #$00


  mov {CONTROL}, #$F0
  mov a,#$30
  mov {TIMER0},a
  mov a,#$60
  mov {tickspeed}, a
  mov a,#$01
  mov {CONTROL}, a
  mov {ticker}, a


  call allnotesoff

  runloop:  
    call checkmsg
  bra runloop



//    dec {ticker}
//    bne .nexttick
//      call dummynote
//      mov a, {tickspeed}
//      mov {ticker}, a
//
//    .nexttick:
//    -;
//    mov y, {COUNTER0}
//    beq {-}




dummynote:
  mov a, {arg0}
  asl a
  mov y, a
  mov a, pitch_table+y
  push y
  mov y, a
  mov a, #{DSP_REG_CH_P_L}
  {dsp_write}
  pop y
  mov a, pitch_table+1+y
  mov y, a
  mov a, #{DSP_REG_CH_P_R}
  {dsp_write}

  mov a, {arg1}
  lsr a
  lsr a

  mov y, a
  mov a, #{DSP_REG_CH_VOL_L}
  {dsp_write}
  mov a, #{DSP_REG_CH_VOL_R}
  {dsp_write}

  mov y, #$00
  mov a, #{DSP_REG_CH_SRCN}
  {dsp_write}

  mov a, {tmp0}
  asl a
  mov x, a

  mov y, #$8F
  mov a, #{DSP_REG_CH_ADSR1}
  {dsp_write}

  mov y, #$EE
  mov a, #{DSP_REG_CH_ADSR2}
  {dsp_write}

  mov y, #$B8
  mov a, #{DSP_REG_CH_GAIN}
  {dsp_write}


  mov y, #$01
  mov a, #{DSP_REG_KON}
  {dsp_write}

  mov a, {tmp0}
  inc a
  cmp a, #$02
  bne {+}
    mov a, #$00
  +;
  mov {tmp0}, a

  ret

wait: //used in between KOF and KON writes
  push a
  mov a, #{waitperiod}
  -; dec a; bne {-}
  pop a
  ret

allnotesoff: //reset notes
  push a
  push y
  mov a, #{DSP_REG_KOF}
  mov y, #$FF
  movw {DSP_ADDR}, ya
  call wait
  mov a, #{DSP_REG_KOF}
  mov y, #$00
  movw {DSP_ADDR}, ya
  pop y
  pop a
  ret


checkmsg:
  push a
  push y
  push x

  //check for new message
  mov a, {strobe_in}
  cmp a, {PORT0}
  beq nomsg

  mov {strobe_in}, {PORT0}

  mov a, {PORT1} //status
  and a, #$F0
  cmp a, #$90
  bne .end
  mov a, {PORT3}
  beq .noteoff

  .noteon:
    mov a, {PORT2}
    mov {arg0}, a
    mov a, {PORT3}
    mov {arg1}, a
    call dummynote
  bra .end
  .noteoff:
    call allnotesoff
  bra .end

  .end:
  mov {PORT0}, {strobe_in}

  nomsg:
  pop x
  pop y
  pop a
  ret

{ExternSPCFragment Start}

incsrc "pitchtable_spc.asm"

incsrc "data_spc/soundbank_spc.asm"

{ExternSPCVector Start}