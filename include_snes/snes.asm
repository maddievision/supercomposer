define args $00
define args1 $01
define args2 $02
define args3 $03

define cursor_x $70
define cursor_y $72
define margin_left $74
define margin_top $76
define margin_right $78
define margin_bottom $7A

define APU_PORT0 $2140
define APU_PORT1 $2141
define APU_PORT2 $2142
define APU_PORT3 $2143

define JOY_PORT0 $4016
define JOY_PORT1 $4017

define A_BYTE sep #$20
define A_WORD rep #$20
define XY_BYTE sep #$10
define XY_WORD rep #$10
define AXY_BYTE sep #$30
define AXY_WORD rep #$30

define ASCII_NUMBER #$30
define ASCII_UCASE #$41

macro hex2str str, number
  {A_BYTE}
  lda.b #{number}
  sta {args2}
  {A_WORD}
  lda.w #{str}
  sta {args0}
  jsl s_hex2str
endmacro

macro print_string str
  {A_WORD}
  lda.w #{str}
  sta {args}
  jsl s_print_string
endmacro

macro spc_load data
  {A_WORD}
  lda.w #({data} & 0xFFFF)
  sta {args}
  lda.w #(({data} >> 16) & 0xFF)
  sta {args2}
  jsl s_spc_load 
endmacro

//BCC <
//BCS >=

macro SPCFragment file, offset, length
  dw {length}, {offset}
  incbin {file}, {offset}, {length}
endmacro

macro SPCVector offset
  dw 0, {offset}
endmacro


macro seek n
  org (({n} & 0x7f0000) >> 1) | ({n} & 0x7fff)
  base {n}
endmacro

macro SnesInit;
        sei;             // Disabled interrupts
        clc;             // clear carry to switch to native mode
        xce;             // Xchange carry & emulation bit. native mode
        rep     #$18;    // Binary mode (decimal mode off), X/Y 16 bit
        ldx.w     #$1FFF;  // set stack to $1FFF
        txs;
        jsr Init;
endmacro



{seek $8000}; fill $010000
{seek $ffea}; dw EmptyHandler   // SNES vectors
{seek $fffc}; dw Start
{seek $8000}

Start:
   {SnesInit}
   jmp main