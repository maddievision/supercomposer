# Building

1. Run the `compile` script from the command line

2. Compiled files will be in the `bin` directory.

  * `spcompo.sfc` Joypad Input (Port 1)
  * `spcompom.sfc` Miracle Piano Input (Port 1)
  * `spcompo.spc` SPC Engine only, for use with an SPC Emulator with MIDI I/O support

# Joypad Input

Buttons are mapped as below:

* `Left` - C
* `Down` - D
* `Up` - E
* `Right` - F
* `Y` - G
* `B` - A
* `X` - B
* `A` - C (Higher Octave)

* `L` - Sharp Switch (+1 Semitone Transpose)
* `R` - Octave Switch (+12 Semitone Transpose)

* `Select` - Decrease Octave
* `Start` - Increase Octave
