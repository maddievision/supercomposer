define midi_in_len $90
define midi_in_byte $92
define midi_in_flag $93
define midi_in_run_status $95
define midi_in_msg $A0

define joy_status $9A

define spc_cmd_strobe $98

macro spc_strobe
      lda {spc_cmd_strobe}
      inc
      bne {+}
      inc
      +
      sta {spc_cmd_strobe}
      sta {APU_PORT0}
endmacro

midi_input_init:
  php
  {A_WORD}
  stz {midi_in_len}
  stz {midi_in_flag}
  stz {midi_in_run_status}
  stz {spc_cmd_strobe}
  plp
  rtl

midi_cmd_init:
  php
  {A_WORD}
  stz {midi_in_len}
  stz {midi_in_flag}
  stz {midi_in_run_status}
  plp
  rtl


read_joy1_status:
  php
  {AXY_BYTE}
  lda.b #$01
  sta {JOY_PORT0}
  lda.b #$00
  nop;nop;nop
  sta {JOY_PORT0}
  sta {joy_status}
  sta {joy_status}+1

  ldx.b #$08
  -;
  lda {JOY_PORT0}
  ror
  rol {joy_status}
  dex
  bne {-}
  ldx.b #$08
  -;
  lda {JOY_PORT0}
  ror
  rol {joy_status}+1
  dex
  bne {-}

  plp
  rtl

read_joy2_status:
  php
  {AXY_BYTE}
  lda.b #$01
  sta {JOY_PORT1}
  lda.b #$00
  nop;nop;nop
  sta {JOY_PORT1}
  sta {joy_status}
  sta {joy_status}+1

  ldx.b #$08
  -;
  lda {JOY_PORT1}
  ror
  rol {joy_status}
  dex
  bne {-}
  ldx.b #$08
  -;
  lda {JOY_PORT1}
  ror
  rol {joy_status}+1
  dex
  bne {-}

  plp
  rtl

send_midi_byte:
  php
  {AXY_BYTE}
  pha
  lda #$01
  sta {JOY_PORT0}
  ldx #$0B
  -; dex
  bne {-}
  pla
  ldx #$08
  rol
  -;
  rol
  sta {JOY_PORT0}
  pha
  lda {JOY_PORT0}
  pla
  dex
  bne {-}
  txa
  sta {JOY_PORT0}
  ldy #$04
  -; dey
  bne {-}  
  plp
  rts

read_midi_byte:
  php
  {XY_WORD}
  ldy #$0000
  {AXY_BYTE}
  lda.b #$01
  sta {JOY_PORT0}
  lda.b #$00
  nop;nop;nop
  sta {JOY_PORT0}
  sta {midi_in_byte}

  lda {JOY_PORT0}
  ror
  bcc .end
  ldx.b #$08
  -;
  lda {JOY_PORT0}
  ror
  rol {midi_in_byte}
  dex
  bne {-}
  ldy {midi_in_len}
  lda {midi_in_byte}
  eor.b #$FF
  sta {midi_in_byte}
  cmp.b #$F0
  bcc .cont

  stz {midi_in_run_status}
  bra .end

  .cont:
  cmp.b #$80
  bcs .newstatus

  cpy #$00
  bne .process

  pha
  lda {midi_in_run_status}
  cmp.b #$80
  bcc .dend
  sta {midi_in_msg},y
  iny
  pla
  bra .process

  .newstatus:
  sta {midi_in_run_status}

  .process:
  sta {midi_in_msg},y
  iny

  sty {midi_in_len}
  lda {midi_in_run_status}
  lsr;lsr;lsr;lsr
  and.b #$0F
  tay
//  lda midi_msg_len,y
  lda {midi_in_len}
  cmp #$03
  bcc .end

  inc {midi_in_flag}

  .end:
  plb
  rtl
  .dend:
  pla
  bra .end

midi_msg_len:
  db 0,0,0,0,0,0,0,0
  db 3,3,3,3,2,2,3,0